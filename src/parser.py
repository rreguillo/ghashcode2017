#!/usr/bin/env python
# -*- coding: utf8 *-

from numpy import *
from objects import *
import pdb

info = {}


def findCache(cid,caches):
    result = find(lambda x: x.cid == cid, caches)
    return result[-1]

def parserInfo(inputfile):
    videos = []
    endpoints = []
    requests =[]
    caches = []
    lindex = 0
    nendpoints = 0
    newEndpoint = True
    with open(inputfile, 'r') as f:
        for line in f:
            if lindex == 0:
                first_line = line
                l1= first_line.split()
                info['V']= int(l1[0])
                info['E']= int(l1[1])
                info['R']= int(l1[2])
                info['C']= int(l1[3])
                info['X']= int(l1[4])
                lindex+=1
            else:
                #CACHES
                caches = map(lambda x: CacheServer(x,-1, []), range(info['C']))
                #VIDEOS
                if len(videos) < info['V']:
                    vids = line.split(" ")
                    videos += map(lambda x: Video(x), vids)
                #ENDPOINTS
                elif len(endpoints) <= info['E']:

                    if newEndpoint:
#                        print len(endpoints)                        
                        endline = line.split(" ")
                        endlat, endncaches = int(endline[0]), int(endline[1].strip())
                        endpoints.append(Endpoint([],[],endlat))
                        endpoints[-1].setCacheLatency(len(caches))
                        newEndpoint = False
                        nendpoints +=1
                        ncacheendpoint = 0
                    else:

                            if ncacheendpoint < endncaches-1:
                                try:
    #                                print "NCACHES < endpoint caches: %d, %d" % (ncacheendpoint, endncaches)
                                    ncacheendpoint += 1
                                    cacheline = line.split(" ")
                                    cacheid,cachelatency = int(cacheline[0]),int(cacheline[1].strip())
                                    endpoints[-1].cachematrix[cacheid] = cachelatency
                                except Exception as e:
                                    print "EXCEPTION %s" % e
                                    pdb.set_trace()
                            elif ncacheendpoint == endncaches-1 :
                                try:
    #                                print "ncache == endcaches %d, %d" % (ncacheendpoint, endncaches)
    #                                cacheline = line.split(" ")
    #                                cacheid,cachelatency = int(cacheline[0]),int(cacheline[1].strip())
    #                                endpoints[-1].cachematrix[cacheid] = cachelatency                            
                                    newEndpoint = True
                                    ncacheendpoint = 0
                                    endncaches = 0
    #                            print "%d, %d" % (ncacheendpoint, endncaches)
                                except Exception as e:
                                    print "EXCEPTION LAsT%s" % e
                                    pdb.set_trace()
                            
                else:
                    lindex +=1
                    try:
                        reqline = line.strip().split(" ")
                        if reqline:
                            idvideo,idendpoint,nrequest = int(reqline[0]), int(reqline[1]), int(reqline[2])
                            requests.append(Request(endpoints[idendpoint],videos[idvideo],nrequest))
                    except Exception as e:
                        pdb.set_trace()
    return endpoints, videos, requests, caches
                            

def putVideos(number):
    for n in range(info['V']):
        print n




ep, vd, re, ca = parserInfo('data/kittens.in')

print len(ep)
print len(vd)
print len(re)
print len(ca)

#putVideos(info['V'])
