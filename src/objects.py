#!/usr/bin/env python
# -*- coding: utf8 *-
import numpy as np

class Video():
    def __init__(self,size,incache=[]):
        self.size = size
        self.incache = []


class Endpoint():
    def __init__(self, users, cacheServers, latency):
        self.users = users
        self.cacheServers = cacheServers
        self.latency = latency
        self.cachematrix = []
        
    def setCacheLatency(self,ncaches):
        self.cachematrix = [0 for x in range(ncaches)]
        
class Request():
    def __init__(self,endpoint,video,times):
        self.endpoint = endpoint
        self.video = video
        self.times = times

class CacheServer():
    def __init__(self,cid, size, videos):
        self.size = size
        self.cid = cid
        self.videos = []
